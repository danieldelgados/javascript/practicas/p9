function realizarOperacion(a1,a2,op){
	var resultado=0;

	a1=Number(a1);
	a2=Number(a2);

	switch(op){
		case '+':
		resultado=a1+a2;
		break;
		case '-':
		resultado=a1-a2;
		break;
		case '*':
		resultado=a1*a2;
		break;
		case '/':
		resultado=a1/a2;
		break;

	}

	return resultado;
}


// var borrar=false;
// var operacion=0;
// var resultado=0;


//variable JSON que tiene una ventaja sobre declarar variables sueltas
//en otros lenguajes de programación se hacen así.

var global={
	borrar:true,
	operacion:0,
	resultado:0
};
/*si quiero añadir otra variable local como por ejemplo un contador
	global.c=0;
*/
window.addEventListener('load',(event)=>{
	var operaciones=document.querySelectorAll('.operacion');
	var numeros=document.querySelectorAll('.numero');
	var pantalla=document.querySelector('.display');

	for(var i=0;i<numeros.length;i++){
		numeros[i].addEventListener('click',(event) => {
	  		if(global.borrar){
	  			global.resultado=Number(pantalla.value);
	  			pantalla.value=event.target.value;
	  			global.borrar=false;
	  		}else{
	  			pantalla.value+=event.target.value;
	  		}
		});
	}
	
	operaciones.forEach((boton) => {
	  boton.addEventListener("click",(event)=>{
	  	if(event.target.value!='='){
	  		if(global.operacion==0){	  		
	  			global.resultado=Number(pantalla.value);
	  		}else{
	  			global.resultado=realizarOperacion(global.resultado,pantalla.value,global.operacion);
			// funcion auto ejecutable
	  		// global.resultado=function(){
	  			//aqui val ecodigo de la funcion
	  		// }(global.resultado,pantalla.value,global.operacion);
	  		
	  		}	  	
	  		global.borrar=true;
	  		global.operacion=event.target.value;	  	
	  		pantalla.value=global.resultado;

	  	}else{
	  		pantalla.value=realizarOperacion(global.resultado,pantalla.value,global.operacion);
	  		global.borrar=true;
	  		global.operacion=0;
	  		global.resultado=0;
	  	}

	  	if(event.target.value=='c'){
	  		pantalla.value=0;
	  		global.borrar=true;
	  		global.operacion=0;
	  		global.resultado=0;
	  	}

	  });
	});

});



